import csv
import subprocess
from os.path import basename
from multiprocessing import Pool
import argparse
import sys


def get_flagstats(file_path):

    print('Calculating stats for', basename(file_path), file=sys.stderr)
    flagstat_bytes = subprocess.check_output(['samtools', 'flagstat', file_path])
    flagstat_output = flagstat_bytes.decode('UTF-8')
    flagstats_raw = flagstat_output.split('\n')[:-1]

    flagstats_nums = [stat[0] for stat in
                      (flagstat_str.split(' ') for flagstat_str in flagstats_raw)]
    flagstats_nums.insert(0, basename(file_path))
    return flagstats_nums


def flagstats_calculator(*file_paths):

    writer = csv.writer(sys.stdout, delimiter='\t', lineterminator='\n')
    stats = ['sample',
             'total',
             'duplicates',
             'mapped',
             'paired',
             'read1',
             'read2',
             'properly paired',
             'with itself and mate mapped',
             'singletons',
             'with mate mapped to a different chr',
             'with mate mapped to a different chr (mapQ>=5)']
    writer.writerow(stats)

    if __name__ == '__main__':
        pool = Pool()
        rows = pool.map(func=get_flagstats, iterable=file_paths)
        pool.close()
        writer.writerows(rows)


def main():
    helpText = ('Takes a number of bam files (can be input with file name globbing), '
                'calculates flagstats in parallel, and outputs to stdout.  Capture the '
                'output to generate a csv file. Note that due to the nature of the '
                'multiprocessing, you may have to hit ctrl-C twice if an error is raised.')
    parser = argparse.ArgumentParser(description=helpText)
    parser.add_argument('input_BAM',
                        help='BAM file for analysis',
                        nargs='+')
    args = parser.parse_args()

    flagstats_calculator(*args.input_BAM)

if __name__ == '__main__':
    main()
