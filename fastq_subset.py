import random
import argparse


def subset(sample, in_fastq_1, in_fastq_2):
    '''Take single- or paired-end fastqs and generate new file(s)
       containing a subset of reads'''

    # Get number of records
    with open(in_fastq_1, 'rU') as f1:
        numLines = sum(1 for line in f1)

    if in_fastq_2:
        with open(in_fastq_2, 'rU') as f2:
            numLines_fastq2 = sum(1 for line in f2)
        assert numLines == numLines_fastq2, 'Paired end files do not contain the same # of lines'

    assert numLines % 4 == 0, 'Number of lines must be a multiple of four.'
    numRecords = numLines / 4
    print 'Number of records in fastq file:', numRecords
    print 'Subset size:', sample

    assert sample < numRecords, 'Fastq contains equal or fewer records than your subsample'

    def recordGenerator(fastq):
        while True:
            yield [next(fastq) for i in xrange(4)]

    random_set = set(random.sample(xrange(numRecords), sample))

    if not in_fastq_2:
        with open(in_fastq_1, 'rU') as f1, open(in_fastq_1 + '.subset', 'w') as fout1:

            for i, record in enumerate(recordGenerator(f1)):
                if i in random_set:
                    fout1.writelines(record)

    else:
        with open(in_fastq_1, 'rU') as f1, open(in_fastq_2, 'rU') as f2, \
                open(in_fastq_1 + '.subset', 'w') as fout1, \
                open(in_fastq_2 + '.subset', 'w') as fout2:

            for i, records in enumerate(zip(recordGenerator(f1), recordGenerator(f2))):
                if i in random_set:
                    fout1.writelines(records[0])
                    fout2.writelines(records[1])


def main():
    helpText = ('Take single- or paired-end fastqs and generate new file(s)'
                'containing a subset of reads')
    parser = argparse.ArgumentParser(description=helpText)
    parser.add_argument('subset_size',
                        help='size of desired subset (required)',
                        type=int,
                        metavar='subset_size')
    parser.add_argument('input_fastq_1',
                        help='paired-end #1 or single-end file (required)',
                        metavar='fastq_1')
    parser.add_argument('input_fastq_2',
                        help='paired end sample #2 (optional)',
                        metavar='fastq_2',
                        nargs='?')
    args = parser.parse_args()

    subset(args.subset_size, args.input_fastq_1, args.input_fastq_2)

if __name__ == '__main__':
    main()
