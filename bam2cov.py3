import pysam
import csv
import concurrent.futures
from multiprocessing import cpu_count
import os

'''
Calculate coverage per-base across a region of a BAM file.
Run this script inside a directory containing the BAM files you want coverage
calculations for.
Output files will be named the same as the BAM files, but with .csv at the end.
(therefore you must name the input BAM files something unique and descriptive!).
Input BAM files must end in .bam

This script requires python3 with the pysam module installed
'''


def get_coverage(input_bam):

    print('Calculating coverage for:', input_bam)

    output_f = input_bam + '.csv'
    with open(output_f, 'w', newline='') as fout:
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        writer.writerow(['chrom', 'position', 'coverage'])

        sam = pysam.Samfile(input_bam, 'rb')

        chrom = '3L'
        start_pos = 7346677  # I am assuming Chuan's coordinates are 1-based
        end_pos = 7357465

        for pileupcolumn in sam.pileup(reference=chrom, start=start_pos, end=end_pos):
            # Have to add this due to what seems to be a pysam bug
            if start_pos <= pileupcolumn.pos <= end_pos:
                writer.writerow(['2L', (pileupcolumn.pos + 1), pileupcolumn.n])

        sam.close()

if __name__ == '__main__':
    bam_files = [f for f in os.listdir('.') if f.endswith('.bam')]
    with concurrent.futures.ProcessPoolExecutor(max_workers=cpu_count()) as executor:
        executor.map(get_coverage, bam_files)
